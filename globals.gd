extends Node

var inventory = {
	"wood":0,
	"stone":0
}

var heat = 100
var hunger = 0

var harvest_power = 1
var heat_power = -1

signal temp_changed(new_temp,delta)

signal inventory_changed(item,quantity)


func set_inventory_item_quantity(item,quantity):
	inventory[item] = quantity
	emit_signal("inventory_changed",item,quantity)
	
func get_inventory_item_quantity(item):
	if !inventory.has(item): return 0
	return inventory[item]
	
func has_item_quantity(item,quantity):
	if !inventory.has(item): return false
	return inventory[item]>=quantity
	
func add_inventory_item(item,quantity):
	if inventory.has(item):
		inventory[item]=inventory[item]+quantity
	else:
		inventory[item]=quantity
	emit_signal("inventory_changed",item,inventory[item])
	
func update_temp(delta):
	heat = heat + delta
	heat = clamp(heat,0,100)
	emit_signal("temp_changed",heat,delta)
	
func check_inventory(recipe):
	var result = {
		"possible" : true,
		"bom" : {}
	}
	for item in recipe:
		var quantity_held = 0
		if inventory.has(item):
			quantity_held = inventory[item]
		if quantity_held < recipe[item]:
			result.possible = false
		result.bom[item] = quantity_held
	return result
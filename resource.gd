extends StaticBody2D

export(int) var resource = 20
export(int) var harvest_roi = 2

export(String) var res_type = ""

var player_in = false
onready var player_area = $PlayerArea
onready var player = preload("res://Player.gd")
var hp = preload("res://HitEffect.tscn")

func _ready():
	connect("input_event",self,"_on_input_event")
	player_area.connect("body_entered",self,"_on_PlayerArea_body_entered")
	player_area.connect("body_exited",self,"_on_PlayerArea_body_exited")
	

func _on_input_event(viewport, event, shape_idx):
	if !player_in: return
	if event is InputEventMouseButton and event.button_index==BUTTON_RIGHT and event.pressed:
		print("harvesting ",res_type)
		var gathered = int(harvest_roi*globals.harvest_power)
		resource = resource - gathered
		if resource<0:
			gathered = gathered+resource
		globals.add_inventory_item(res_type,gathered)
		var hpi = hp.instance()
		hpi.set_position(position)
		get_parent().add_child(hpi)
		hpi.set_label(gathered)
		#hpi.set_position(position)
	if resource <= 0:
		queue_free()

func _on_PlayerArea_body_entered(body):
	if body is player:
		player_in = true


func _on_PlayerArea_body_exited(body):
	if body is player:
		player_in = false

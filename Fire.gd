extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var player = preload("res://Player.gd")

var affected_player = null


export var resources = 100

func _ready():
	pass




func _on_Area2D_body_entered(body):
	if body is player:
		affected_player = body


func _on_Area2D_body_exited(body):
	if body is player:
		affected_player = null


func _on_Timer_timeout():
	resources = resources - 3
	print("Fire:",str(resources))
	if affected_player!=null:
		globals.update_temp(3)
	if resources<0:
		queue_free()
extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var resource = 20

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass



func _on_Tree_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index==BUTTON_RIGHT and event.pressed:
		print("harvesting wood")
		globals.add_inventory_item("wood",2)
		resource = resource - 2
	if resource <= 0:
		queue_free()

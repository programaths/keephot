extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var looking_at = 0

func _process(delta):
	var move = Vector2(0,0)
	if Input.is_action_pressed("ui_up"):
		move.y = -1
	if Input.is_action_pressed("ui_down"):
		move.y=1
	if Input.is_action_pressed("ui_left"):
		move.x = -1
	if Input.is_action_pressed("ui_right"):
		move.x=1
		
	move = move.normalized() * 256
	move_and_slide(move)
	if move.x !=0 or move.y!=0:
		looking_at = move.angle()
	rotation = looking_at



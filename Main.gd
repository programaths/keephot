extends Node

onready var craft_dialog = $CanvasLayer/Crafting
onready var player = $Player

onready var fire_spawner = preload("res://FireSpawner.tscn")

var current_spawner = null

func _input(event):
	if event is InputEventKey and event.is_action("ui_craft"):
		craft_dialog.show()


func _on_Crafting_popup_hide():
	pass # replace with function body


func _on_Timer_timeout():
	globals.update_temp(globals.heat_power)


func _on_HUD_basic_fire_button_clicked():
	if current_spawner != null : return
	if globals.has_item_quantity("basic_fire",1):
		current_spawner = fire_spawner.instance()
		add_child(current_spawner)
		current_spawner.set_position(player.position)
		yield(current_spawner,"tree_exiting")
		current_spawner = null
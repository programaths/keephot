extends ReferenceRect

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

onready var wood = $VBoxContainer/Panel/HBoxContainer/wood_counter
onready var stone = $VBoxContainer/Panel/HBoxContainer/stone_counter
onready var temp_indic = $VBoxContainer/TextureProgress
onready var tween = $Tween



signal basic_fire_button_clicked

func _ready():
	globals.connect("inventory_changed",self,"on_resource_update")
	globals.connect("temp_changed",self,"on_temp_changed")

func on_resource_update(item,amount):
	match item:
		"wood": wood.text = str(amount)
		"stone" : stone.text = str(amount)

func on_temp_changed(new_temp,delta):
	tween.interpolate_property(temp_indic, "value",
                temp_indic.value, new_temp, 1,
                Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()

	#temp_indic.value = new_temp
	print(new_temp)

	


func _on_BasicFireButton_pressed():
	emit_signal("basic_fire_button_clicked")

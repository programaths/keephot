extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var fire = preload("res://Fire.tscn")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	move_and_slide(get_local_mouse_position()*2)


func _on_FireSpawner_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if globals.get_inventory_item_quantity("basic_fire")>0:
				globals.add_inventory_item("basic_fire",-1)
				var fire2place = fire.instance()
				get_parent().add_child(fire2place)
				fire2place.set_position(position)
			queue_free()
			pass
		elif event.button_index == BUTTON_RIGHT:
			queue_free()

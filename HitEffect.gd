extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

onready var tween = $Tween
onready var label = $Label


func _ready():
	tween.interpolate_property(self, "position",
                position, position+Vector2(0,-32), 1,
                Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()


func set_label(l):
	label.text = "+ " + str(l)


func _on_Tween_tween_completed(object, key):
	queue_free()
	print("done")
extends WindowDialog

# class member variables go here, for example:
# var a = 2
# var b = "textvar"


onready var selector = $VBoxContainer/HSplitContainer/ScrollContainer/ItemList
onready var description = $VBoxContainer/HSplitContainer/CenterContainer/VBoxContainer/RichTextLabel
onready var btn_craft = $VBoxContainer/HBoxContainer/ButtonCraft

var tree = preload("res://Wood.png")
var stone = preload("res://Rocks.png")
var fire = preload("res://Fire.png")

var needs = [
	{"wood":4,"stone":4},
	{"wood":16,"stone":6}
]

var items = [
	"basic_fire",
	"loaded_fire"
]

var index_to_craft = -1

func show():
	for i in range(0,selector.get_item_count()):
		selector.unselect(i)
	description.clear()
	.show()

func _ready():
	selector.add_item("Basic Fire",fire)
	selector.add_item("Loaded Fire",fire)
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_ItemList_item_selected(index):
	var name = selector.get_item_text(index)
	description.clear()
	description.append_bbcode("[b]"+name+"[b]")
	var recipe = needs[index]
	var check_result = globals.check_inventory(recipe)
	var bom = check_result.bom
	description.push_indent(1)
	for item in check_result.bom:
		description.add_text(item+": ")
		if bom[item] < recipe[item]:
			description.push_color(ColorN("red"))
		else:
			description.push_color(ColorN("green"))
		description.add_text(str(bom[item]))
		description.pop()
		description.add_text("/"+str(recipe[item]))
		description.newline()
	description.pop()
	btn_craft.disabled = !check_result.possible
	if check_result.possible:
		index_to_craft = index
	else:
		index_to_craft = -1

func _on_ButtonCacnel_pressed():
	hide()


func _on_ButtonCraft_pressed():
	if index_to_craft == -1 : return
	var recipe = needs[index_to_craft]
	for item in recipe:
		globals.add_inventory_item(item,-recipe[item])
	globals.add_inventory_item(items[index_to_craft],1)
	_on_ItemList_item_selected(index_to_craft)

